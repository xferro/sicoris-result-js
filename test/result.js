var expect = require('expect.js'),
    sinon = require('sinon'),
    Result = require('../src/result.js');

describe('Result.success', function() {
    var result = Result.success("hello");
    
    it("isFailure returns false", function(done) {
        expect(result.isFailure()).to.be(false);
        done();
    });

    it('isSuccess returns true', function(done) {
        expect(result.isSuccess()).to.be(true);
        done();
    });

    it('exceptionOrNull returns null', function(done) {
        expect(result.exceptionOrNull()).to.be(null);
        done();
    });

    it('getOrNull to return the encapsulated value', function(done) {
        expect(result.getOrNull()).to.be("hello");
        done();
    });

    it('fold returns the encapsulated value', function(done) {
        var fn = function(value) { return value + value; };
        expect(result.fold(fn)).to.be("hellohello");
        done();
    });
    
    it('getOrDefault to return the encapsulated value', function(done) {
        expect(result.getOrDefault("default")).to.be("hello");
        done();
    });

    it('getOrElse to return the encapsulated value', function(done) {
        expect(result.getOrElse()).to.be("hello");
        done();
    });

    it('getOrThrow to return the encapsulated value', function(done) {
        expect(result.getOrThrow()).to.be("hello");
        done();
    });

    it('map returns the result of transforming the encapsulated value', function(done) {
        var fn = function(value) { return value + value; };
        expect(result.map(fn).getOrNull()).to.be("hellohello");
        done();
    });

    it('onFailure is not called', function(done) {
        var fn = function() {
            done(new Error("function should not be called"));
        };

        expect(result.onFailure(fn).getOrNull()).to.be("hello");
        done();
    });

    it('onSuccess is called', function(done) {
        var x;
        var fn = function() {
            x = "set";
        };

        expect(result.onSuccess(fn).getOrNull()).to.be("hello");
        expect(x).to.be("set");
        done();
    });

    it('recover is not called', function(done) {
        var fn = function() {
            done(new Error("function should not be called"));
        };

        expect(result.recover(fn).getOrNull()).to.be("hello");
        done();
    });

});

describe('Result.failure', function() {
    var error = new Error("fatal");
    var result = Result.failure(error);
    
    it("isFailure returns true", function(done) {
        expect(result.isFailure()).to.be(true);
        done();
    });

    it('isSuccess returns false', function(done) {
        expect(result.isSuccess()).to.be(false);
        done();
    });

    it('exceptionOrNull returns excetpion', function(done) {
        expect(result.exceptionOrNull()).to.be(error);
        done();
    });

    it('getOrNull to return null', function(done) {
        expect(result.getOrNull()).to.be(null);
        done();
    });

    it('fold returns the value from the error function', function(done) {
        var fn = function(exception) { return "terror"; };
        expect(result.fold(null, fn)).to.be("terror");
        done();
    });
    
    it('getOrDefault to return the default value', function(done) {
        expect(result.getOrDefault("default")).to.be("default");
        done();
    });

    it('getOrElse to return the value returned from calling the onFailure function', function(done) {
        expect(result.getOrElse(function() { return "else";})).to.be("else");
        done();
    });

    it('getOrThrow to throw the exception', function(done) {
        try {
            expect(result.getOrThrow()).to.be("hello");
            done(new Error("it had to throw the error"));
        } catch (err) {
            expect(err).to.be(error);
            done();
        }
    });

    it('map does nothing', function(done) {
        var fn = function(value) { return value + value; };
        expect(result.map(fn).isFailure()).to.be(true);
        done();
    });

    it('onFailure is called', function(done) {
        var x;
        var fn = function() {
            x = "set";
        };

        expect(result.onFailure(fn).getOrNull()).to.be(null);
        expect(x).to.be("set");
        done();
    });

    it('onSuccess is not called', function(done) {
        var fn = function() {
            done(new Error("should not be called"));
        };

        expect(result.onSuccess(fn).getOrNull()).to.be(null);
        done();
    });

    it('recover should be called', function(done) {
        var fn = function(value) {
            return "recovered";
        };

        expect(result.recover(fn).getOrNull()).to.be("recovered");
        done();
    });

});
