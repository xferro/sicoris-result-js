# sicoris-result-js

JS implementation of the Kotlin Result concept concept. 

## Installation

This module is installed via npm:

``` bash
$ npm install sicoris-result-js
```

## Example Usage

``` js
var Result = require('sicoris-result-js');

// orElse usage
var successString = "yes";
var recoverString = "recovered";
var errorObject = new Error("no");

var successFn = function(value) {
    return value + value;
};

var failureFn = function(error) {
    return recoverString;
};

var success = Result.success(successString);
var failure = Result.failure(errorObject);

// isSuccess
console.log(success.isSuccess()); // > true
console.log(success.isFailure()); // > false

console.log(failure.isSuccess()); // > false
console.log(failure.isFailure()); // > true

// exceptionOrNull
console.log(success.exceptionOrNull()); // > null
console.log(failure.exceptionOrNull()); // > errorObject

// getOrNull
console.log(success.getOrNull()); // "yes"
console.log(failure.getOrNull()); // null

// fold
console.log(success.fold(successFn, null)); // "yesyes"
console.log(failure.fold(null, failureFn)); // "recovered"

// getOrDefault
console.log(success.getOrDefault("default")); // "yes"
console.log(failure.getOrDefault("default")); // "default"

// getOrElse
console.log(success.getOrElse("default")); // "yes"
console.log(failure.getOrElse(failureFn)); // "recovered"

// getOrThrow
console.log(success.getOrThrow("default")); // "yes"
console.log(failure.getOrThrow(failureFn)); // throws the Error

// map
console.log(success.map(successFn).getOrDefault("default")); // "yesyes"
console.log(failure.map(failureFn).getOrDefault("default")); // "default"

// recover
console.log(success.recover(successFn).getOrDefault("default")); // "yes"
console.log(failure.recover(failureFn).getOrDefault("default")); // "recovered"

```
There are two other methods onSuccess and onFailure that are used for applying effects to results.