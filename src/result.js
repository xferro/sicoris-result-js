/*jslint node: true */
'use strict';

module.exports = {
    failure: function(exception) {
        return new Result(false, exception);
    },
    success: function(value) {
        return new Result(true, value);
    }
};

var isNullOrUndefined = function(value) {
    return (value === undefined || value === null);
};

var isFunction = function(value) {
    return typeof value === 'function';
};

var assertIsAFunction = function(fn, errorMessage) {
    if (isNullOrUndefined(fn) || !isFunction(fn)) {
        throw new Error(errorMessage);
    }
};

var Result = function(isSuccessful, value) {
    /**
     * Returns true if this instance represents failed outcome. In this case isSuccess returns false.
     */
    this.isFailure = function() {
        return !isSuccessful;
    };

    /**
     * Returns true if this instance represents successful outcome. In this case isFailure returns false
     */
    this.isSuccess = function() {
        return isSuccessful;
    };

    /**
     * Returns the encapsulated exception if this instance represents failure or null if it is success.
     */
    this.exceptionOrNull = function() {
        return (!isSuccessful) ? value : null;
    };

    /**
     * Returns the encapsulated value if this instance represents success or null if it is failure.
     */
    this.getOrNull = function() {
        return (isSuccessful) ? value : null;
    };

    /**
     * Returns the the result of onSuccess for encapsulated value if this instance represents success or 
     * the result of onFailure function for encapsulated exception if it is failure.
     */
    this.fold = function(onSuccess, onFailure) {
        return (isSuccessful) ? onSuccess(value) : onFailure(value);
    };

    /**
     * Returns the encapsulated value if this instance represents success or the defaultValue if it is failure.
     */
    this.getOrDefault = function(defaultValue) {
        return (isSuccessful) ? value : defaultValue;
    };

    /** 
     * Returns the encapsulated value if this instance represents success or the result of onFailure function 
     * for encapsulated exception if it is failure.
     */
    this.getOrElse = function(onFailure) {
        return (isSuccessful) ? value : onFailure(value);
    };

    /**
     * Returns the encapsulated value if this instance represents success or throws the encapsulated 
     * exception if it is failure.
     */
    this.getOrThrow = function() {
        if (isSuccessful) {
            return value;
        } else {
            throw value;
        }
    };

    /**
     * Returns the encapsulated result of the given transform function applied to encapsulated value if this instance 
     * represents success or the original encapsulated exception if it is failure.
     */
    this.map = function(transform) {
        return (isSuccessful) ? new Result(true, transform(value)) : this;
    };

    /**
     * Performs the given action on encapsulated exception if this instance represents failure. 
     * Returns the original Result unchanged.
     * 
     * @param {*} action which is a function(Throwable)
     */
    this.onFailure = function(action) {
        if (!isSuccessful) {
            action(value);
        }

        return this;
    };

    /**
     * Performs the given action on encapsulated value if this instance represents success. 
     * Returns the original Result unchanged.
     * 
     * @param {*} action which is a function(value)
     */
    this.onSuccess = function(action) {
        if (isSuccessful) {
            action(value);
        }

        return this;
    };

    /**
     * Returns the encapsulated result of the given transform function applied to encapsulated 
     * exception if this instance represents failure or the original encapsulated value if it is success.
     * 
     * @param {*} transform 
     */
    this.recover = function(transform) {
        if (!isSuccessful) {
            return new Result(true, transform(value));
        } else {
            return this;
        }
    };
};
